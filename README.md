# LeVanChien-Database
- Nắm được khái niệm Database
- Biết cách vẽ sơ đồ ER
- Biết cách tạo `database` trên `mysql server`, cách tạo bảng, các kiểu dữ liệu
- Thực hiện truy vấn mysql:
 - `select`, `update`, `delete`
 - `join` và các loại join 
 - ...
- Tối ưu câu truy vấn
